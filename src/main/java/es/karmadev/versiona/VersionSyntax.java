package es.karmadev.versiona;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents a version syntax
 * This class tries to simplify the logic
 * when trying to parse or read version strings,
 * by providing a set of predefined rules to
 * work with versions.
 * The rules are the following:
 * <code>
 *     m = mayor (int)
 *     n = minor (int)
 *     p = patch (int)
 *     b = build (text)
 * </code>
 * When defining the rule "p" twice, the second
 * "patch" will be considered as a "minor patch".
 * For instance, if your software uses a version
 * schema of X.X.X.X, this class will be completely
 * compatible with that schema.
 * VersionSyntax will always {@link #resolve resolve}
 * a version string, regardless of the defined rule, but
 * {@link #isValidVersion validation check} will fail
 * if the version string does not meet the required rules.
 * To fail if the version string does not meet the required rules
 * and the version string gets parsed, the method {@link #tryResolve try to resolve}
 * should be used instead
 */
@SuppressWarnings("unused")
public final class VersionSyntax {

    private final static String MAYOR = "(?<mayor>[0-9]+)";
    private final static String MINOR = "(?<minor>\\.?[0-9]+)";
    private final static String PATCH = "(?<patch>\\.?[0-9]+)";
    private final static String MINOR_PATCH = "(?<minorPatch>\\.?[0-9]+)";
    private final static String BUILD = "(?<build>-?[a-zA-Z0-9-_.,]*)";

    public final static VersionSyntax SINGLE = VersionSyntax.compile('m');
    public final static VersionSyntax SIMPLE = VersionSyntax.compile('m', 'n');
    public final static VersionSyntax SHORT = VersionSyntax.compile('m', 'n', 'p');
    public final static VersionSyntax LONG = VersionSyntax.compile('m', 'n', 'p', 'p');
    public final static VersionSyntax SHORT_BUILD = VersionSyntax.compile('m', 'n', 'p', 'b');
    public final static VersionSyntax LONG_BUILD = VersionSyntax.compile('m', 'n', 'p', 'p', 'b');

    private final Pattern pattern;
    private final Pattern strict;

    /**
     * Create a new version syntax
     *
     * @param rules the version rules
     */
    private VersionSyntax(final char[] rules) {
        StringBuilder patternBuilder = new StringBuilder();

        Map<String, Boolean> ruleMap = new LinkedHashMap<>();
        ruleMap.put(MAYOR, false);
        ruleMap.put(MINOR, false);
        ruleMap.put(PATCH, false);
        ruleMap.put(MINOR_PATCH, false);
        ruleMap.put(BUILD, false);

        for (char character : rules) {
            switch (character) {
                case 'm':
                    ruleMap.put(MAYOR, true);
                    break;
                case 'n':
                    ruleMap.put(MINOR, true);
                    break;
                case 'p':
                    if (ruleMap.get(PATCH)) {
                        ruleMap.put(MINOR_PATCH, true);
                    } else {
                        ruleMap.put(PATCH, true);
                    }
                    break;
                case 'b':
                    ruleMap.put(BUILD, true);
                    break;
            }
        }

        StringBuilder strictBuilder = new StringBuilder();
        String previous = null;
        for (String key : ruleMap.keySet()) {
            boolean add = ruleMap.get(key);
            if (!key.equals(MAYOR)) {
                if (!ruleMap.get(previous)) {
                    assert previous != null;
                    patternBuilder.append(previous).append('?');
                }

                previous = key;
                if (add) {
                    patternBuilder.append((key.equals(BUILD) ? '-' : "\\.?"))
                            .append(key.replace("\\.?", "")
                                    .replace("-?", ""));

                    strictBuilder.append((key.equals(BUILD) ? '-' : "\\.?"))
                            .append(key.replace("\\.?", "")
                                    .replace("-?", ""));
                }

                continue;
            }

            previous = key;
            if (add) {
                patternBuilder.append(key);
                strictBuilder.append(key);
            }
        }

        assert previous != null;

        if (!ruleMap.get(previous)) {
            patternBuilder.append(previous).append('?');
        }

        this.pattern = Pattern.compile(patternBuilder.toString());
        this.strict = Pattern.compile(strictBuilder.toString());
    }

    /**
     * Get the version syntax
     *
     * @return the syntax
     */
    public char[] getSyntax() {
        String raw = this.strict.pattern();
        List<Character> chars = new ArrayList<>();

        if (raw.contains("mayor")) chars.add('m');
        if (raw.contains("minor")) chars.add('n');
        if (raw.contains("patch")) chars.add('p');
        if (raw.contains("minorPatch")) chars.add('p');
        if (raw.contains("build")) chars.add('b');

        char[] rules = new char[chars.size()];
        for (int i = 0; i < chars.size(); i++) {
            rules[i] = chars.get(i);
        }

        return rules;
    }

    /**
     * Get if the version matches the
     * version syntax pattern
     *
     * @param version the version
     * @return if the version matches
     * the version syntax
     */
    public boolean isValidVersion(final String version) {
        return this.strict.matcher(version).matches();
    }

    /**
     * Resolve the version
     *
     * @param version the version
     * @return the resolved version
     */
    public Version resolve(final String version) {
        return resolve(this.pattern, version);
    }

    /**
     * Tries to resolve the version
     * or return null if the version string
     * does not meet the required rules
     *
     * @param version the version
     * @return the resolved version
     */
    public Version tryResolve(final String version) {
        return resolve(this.strict, version);
    }

    private Version resolve(final Pattern pattern, final String version) {
        Matcher matcher = this.pattern.matcher(version);
        if (!matcher.matches() && !matcher.find()) return null;

        String rawMayor = tryGroup(matcher, "mayor");
        String rawMinor = tryGroup(matcher, "minor");
        String rawPatch = tryGroup(matcher, "patch");
        String rawMinorPatch = tryGroup(matcher, "minorPatch");
        String build = tryGroup(matcher, "build");

        int mayor = parseInt(rawMayor);
        int minor = parseInt(rawMinor);
        int patch = parseInt(rawPatch);
        int minorPatch = parseInt(rawMinorPatch);

        return Version.build(mayor, minor, patch, minorPatch, build);
    }

    private static String tryGroup(final Matcher matcher, final String group) {
        try {
            return matcher.group(group);
        } catch (IllegalArgumentException ignored) {}
        return null;
    }

    private static int parseInt(final String str) {
        if (str == null) return -1;
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            return -1;
        }
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * <p>
     * The {@code equals} method implements an equivalence relation
     * on non-null object references:
     * <ul>
     * <li>It is <i>reflexive</i>: for any non-null reference value
     *     {@code x}, {@code x.equals(x)} should return
     *     {@code true}.
     * <li>It is <i>symmetric</i>: for any non-null reference values
     *     {@code x} and {@code y}, {@code x.equals(y)}
     *     should return {@code true} if and only if
     *     {@code y.equals(x)} returns {@code true}.
     * <li>It is <i>transitive</i>: for any non-null reference values
     *     {@code x}, {@code y}, and {@code z}, if
     *     {@code x.equals(y)} returns {@code true} and
     *     {@code y.equals(z)} returns {@code true}, then
     *     {@code x.equals(z)} should return {@code true}.
     * <li>It is <i>consistent</i>: for any non-null reference values
     *     {@code x} and {@code y}, multiple invocations of
     *     {@code x.equals(y)} consistently return {@code true}
     *     or consistently return {@code false}, provided no
     *     information used in {@code equals} comparisons on the
     *     objects is modified.
     * <li>For any non-null reference value {@code x},
     *     {@code x.equals(null)} should return {@code false}.
     * </ul>
     * <p>
     * The {@code equals} method for class {@code Object} implements
     * the most discriminating possible equivalence relation on objects;
     * that is, for any non-null reference values {@code x} and
     * {@code y}, this method returns {@code true} if and only
     * if {@code x} and {@code y} refer to the same object
     * ({@code x == y} has the value {@code true}).
     * <p>
     * Note that it is generally necessary to override the {@code hashCode}
     * method whenever this method is overridden, so as to maintain the
     * general contract for the {@code hashCode} method, which states
     * that equal objects must have equal hash codes.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     * @see #hashCode()
     * @see HashMap
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof VersionSyntax)) return false;
        VersionSyntax other = (VersionSyntax) obj;

        return other.pattern.equals(this.pattern);
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link HashMap}.
     * <p>
     * The general contract of {@code hashCode} is:
     * <ul>
     * <li>Whenever it is invoked on the same object more than once during
     *     an execution of a Java application, the {@code hashCode} method
     *     must consistently return the same integer, provided no information
     *     used in {@code equals} comparisons on the object is modified.
     *     This integer need not remain consistent from one execution of an
     *     application to another execution of the same application.
     * <li>If two objects are equal according to the {@code equals(Object)}
     *     method, then calling the {@code hashCode} method on each of
     *     the two objects must produce the same integer result.
     * <li>It is <em>not</em> required that if two objects are unequal
     *     according to the {@link Object#equals(Object)}
     *     method, then calling the {@code hashCode} method on each of the
     *     two objects must produce distinct integer results.  However, the
     *     programmer should be aware that producing distinct integer results
     *     for unequal objects may improve the performance of hash tables.
     * </ul>
     * <p>
     * As much as is reasonably practical, the hashCode method defined by
     * class {@code Object} does return distinct integers for distinct
     * objects. (This is typically implemented by converting the internal
     * address of the object into an integer, but this implementation
     * technique is not required by the
     * Java&trade; programming language.)
     *
     * @return a hash code value for this object.
     * @see Object#equals(Object)
     * @see System#identityHashCode
     */
    @Override
    public int hashCode() {
        return this.pattern.hashCode();
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return this.pattern.pattern();
    }

    /**
     * Create a new version syntax
     *
     * @param rules the version rules
     * @return the version syntax
     */
    public static VersionSyntax compile(final char... rules) {
        return new VersionSyntax(rules);
    }
}