# Versiona
Version is a library to fetch and manage software versions.
The library comes out of the box ready to handle multiple known
version syntaxes, such as the microsoft long version format
(`x`.`y`.`z`.`i`) and the most common version format (`x`.`y`.`z`).

The library also has support for version data, for example;
`1.0.0-canary2024`.

# Usage
The library usage is very simple, you can create a version
instance through its static builder method.

## Version
```java
import es.karmadev.versiona.Version;

public static void main(String[] args) {
    Version version = Version.build(1, 0, 0);
    System.out.println(version); //Prints 1.0.0
}
```

```java
import es.karmadev.versiona.Version;

public static void main(String[] args) {
    Version version = Version.build(1, 0, 0, "canary2024");
    System.out.println(version); //Prints 1.0.0-canary2024
}
```

```java
import es.karmadev.versiona.Version;

public static void main(String[] args) {
    Version version = Version.build("1.2.3");
    System.out.println(version.getMayor()); //Prints 1
    System.out.println(version.getMinor()); //Prints 2
    System.out.println(version.getPatch()); //Prints 3
}
```

## VersionSyntax
The version syntax is used when you want to be more specific
about which version syntax to parse. There are 6 prebuild
syntaxes.

```java
import es.karmadev.versiona.VersionSyntax;

public static volatile main(String[] args) {
    VersionSyntax single = VersionSyntax.SINGLE;
    //Prefers versions of a single number (1)
    
    VersionSyntax simple = VersionSyntax.SIMPLE;
    //Prefers versions of two numbers (1.0)
    
    VersionSyntax shortSyntax = VersionSyntax.SHORT;
    //Prefers versions of three numbers (1.0.0)
    
    VersionSyntax shortSyntaxB = VersionSyntax.SHORT_BUILD;
    //Prefers versions of three numbers with 
    //build data (1.0.0-canary2024)

    VersionSyntax shortSyntax = VersionSyntax.LONG;
    //Prefers versions of four numbers (1.0.0.0)

    VersionSyntax shortSyntaxB = VersionSyntax.LONG_BUILD;
    //Prefers versions of four numbers with 
    //build data (1.0.0.0-canary2024)
}
```

You can use the `#resolve(String)` method of `VersionSyntax` in
order to resolve a version string into a Version object. The
resolve method will ALWAYS try to resolve the version, and
fill the missing version fields with default values (zero for
numbers, and null for build data). If you want to be more
restrictive about resolving a version, the method `#tryResolve(String)`
is what you are looking for. The tryResolve method tries to
resolve the version string, but if it does not meet the
syntax rules, it will return null instead of generating the
missing values.

# Fetching versions
The library comes with a simple version fetcher. This version 
fetcher supports markdown and json formats, but it allows
extensions of formats to be added.

## Fetching json
The library has a schema defined for parsing versions.
```json
{
	"$schema": "http://json-schema.org/draft-04/schema#",
	"type": "object",
	"properties": {
		"versions": {
			"type": "array",
			"items": {
				"type": "object",
				"patternProperties": {
					"^[0-9]+(\\.[0-9]+)*$": {
						"type": "object",
						"properties": {
							"date": {
								"type": "string"
							},
							"build": {
								"type": "string"
							},
							"update": {
								"type": "array",
								"items": {
									"type": "string",
									"pattern": "^(https?://).+$"
								}
							},
							"changelog": {
								"type": "array",
								"items": {
									"type": "string"
								}
							}
						},
						"required": [
							"date",
							"build",
							"changelog"
						]
					}
				}
			}
		}
	},
	"required": [
		"versions"
	]
}
```
If you fail at some point of the schema, the default
VersionFetcher will simply fail to parse. It's very important
that you follow the schema rules, or use markdown. Here's
an example of a json version

```json
{
    "versions": [
		{
            "1.0.0": {
                "date": "2023-10-09T20:54:05.445Z",
                "build": "RELEASE",
                "update": [
                    "https://mywebsite.com/updates/v1.0.0/application.jar",
					"https://fallback.mywebsite.com/updates/v1.0.0/application.jar"
                ],
                "changelog": [
                    "Some changelog line",
                    "another changelog line"
                ]
            },
            "1.0.0-canary": {
				"date": "2023-10-09T20:54:05.445Z",
                "build": "SNAPSHOT",
				"changelog": [
					"Some changelog line",
					"another changelog line"
				]
            }
        },
		{
            "0.9.9": {
				"date": "2023-10-09T20:54:05.445Z",
				"build": "SNAPSHOT",
				"changelog": [
					"Some changelog line",
					"another changelog line"
				]
            } 
        }
    ]
}
```

```java
import es.karmadev.versiona.data.VersionHistory;
import es.karmadev.versiona.fetch.VersionFetcher;

public static void main(String[] args) {
    VersionFetcher fetcher = new VersionFetcher("https://mywebsite.com/versions.json");
    VersionHistory history = fetcher.fetch();
}
```

## Fetching markdown
Markdown is easier to use, and if your server does not support
markdown, don't worry! VersionFetcher will try to parse txt 
files as markdown. The best part about markdown fetching is
that the only rule is the order you define the versions and
the changelogs. For instance, you CAN'T write first the
changelog and then the version, otherwise the application
will fail to fetch. Here's an example of markdown

```md
# 1.0.0
## RELEASE
### 2023-10-09T20:54:05.445Z
[https://mywebsite.com/updates/v1.0.0/application.jar]()
[https://fallback.mywebsite.com/updates/v1.0.0/application.jar]()
- Some changelog line
- another changelog line

# 1.0.0-canary
## SNAPSHOT
### 2023-10-09T20:54:05.445Z
- Some changelog line
- another changelog line

# 0.9.9
## SNAPSHOT
### 2023-10-09T20:54:05.445Z
- Some changelog line
- another changelog line
```

```java
import es.karmadev.versiona.data.VersionHistory;
import es.karmadev.versiona.fetch.VersionFetcher;

public static void main(String[] args) {
    VersionFetcher fetcher = new VersionFetcher("https://mywebsite.com/versions.md");
    //Remember that you can also use txt files formatted as markdown
    VersionHistory history = fetcher.fetch();
}
```

One of the advantages of using markdown is if your applications
uses markdown to display changelog information, as markdown 
can easily show and format data. For instance, the
previous markdown example would look like this in a 
application which supports markdown syntax:

#### Markdown preview
# 1.0.0
## RELEASE
### 2023-10-09T20:54:05.445Z
[https://mywebsite.com/updates/v1.0.0/application.jar]()
[https://fallback.mywebsite.com/updates/v1.0.0/application.jar]()
- Some changelog line
- another changelog line

# 1.0.0-canary
## SNAPSHOT
### 2023-10-09T20:54:05.445Z
- Some changelog line
- another changelog line

# 0.9.9
## SNAPSHOT
### 2023-10-09T20:54:05.445Z
- Some changelog line
- another changelog line