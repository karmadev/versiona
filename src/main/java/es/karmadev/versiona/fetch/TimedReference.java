package es.karmadev.versiona.fetch;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

/**
 * Represents a cache element
 * which gets "removed" after the
 * specified time
 */
@SuppressWarnings("unused")
public class TimedReference<T> extends WeakReference<T> {

    private final long originalTTL;

    private long timeToLive;
    private T reference;

    /**
     * Creates a new weak reference that refers to the given object.  The new
     * reference is not registered with any queue.
     *
     * @param referent object the new weak reference will refer to
     * @param timeToLive the time the reference well be keep
     */
    public TimedReference(final T referent, final long timeToLive) {
        this(referent, timeToLive, TimeUnit.MILLISECONDS);
    }

    /**
     * Creates a new weak reference that refers to the given object.  The new
     * reference is not registered with any queue.
     *
     * @param timeToLive the time the reference well be keep
     */
    public TimedReference(final long timeToLive) {
        this(null, timeToLive, TimeUnit.MILLISECONDS);
    }

    /**
     * Creates a new weak reference that refers to the given object.  The new
     * reference is not registered with any queue.
     *
     * @param timeToLive the time the reference well be keep
     * @param unit the time unit used for TTL
     */
    public TimedReference(final long timeToLive, final TimeUnit unit) {
        this(null, timeToLive, unit);
    }

    /**
     * Creates a new weak reference that refers to the given object.  The new
     * reference is not registered with any queue.
     *
     * @param referent object the new weak reference will refer to
     * @param timeToLive the time the reference well be keep
     * @param unit the time unit used for TTL
     */
    public TimedReference(final T referent, final long timeToLive, final TimeUnit unit) {
        super(referent);
        this.reference = referent;
        this.originalTTL = TimeUnit.MILLISECONDS.convert(timeToLive, unit);
        this.timeToLive = System.currentTimeMillis() + originalTTL;
    }

    /**
     * Get the reference TTL
     *
     * @return the TTL
     */
    public long getTTL() {
        return this.timeToLive;
    }

    /**
     * Returns this reference object's referent.  If this reference object has
     * been cleared, either by the program or by the garbage collector, then
     * this method returns <code>null</code>.
     *
     * @return The object to which this reference refers, or
     * <code>null</code> if this reference object has been cleared
     */
    @Override
    public T get() {
        super.clear();
        return this.reference;
    }

    /**
     * Refresh the reference
     *
     * @return if the action was success
     */
    public boolean refresh() {
        if (timeToLive > System.currentTimeMillis())
            return false;

        this.timeToLive = System.currentTimeMillis() + originalTTL;
        return true;
    }

    /**
     * Update the reference
     *
     * @param newReferent the new referent
     */
    public void update(final T newReferent) {
        this.reference = newReferent;
        this.timeToLive = System.currentTimeMillis() + originalTTL;
    }

    /**
     * Clears this reference object.  Invoking this method will not cause this
     * object to be enqueued.
     *
     * <p> This method is invoked only by Java code; when the garbage collector
     * clears references it does so directly, without invoking this method.
     */
    @Override
    public void clear() {
        if (timeToLive > System.currentTimeMillis())
            return;

        this.reference = null;
    }
}
