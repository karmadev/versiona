package es.karmadev.versiona.fetch;

import es.karmadev.versiona.data.VersionHistory;
import es.karmadev.versiona.fetch.exception.ServerError;

import java.io.InputStream;

/**
 * Represents a version resolver. The
 * version resolver allows to extend {@link VersionFetcher}
 * by providing more content type methods
 * and/or parse regardless of the content-type
 */
@FunctionalInterface @SuppressWarnings("unused")
public interface VersionResolver {

    /**
     * Resolve the version
     *
     * @param contentType the server response content
     *                    type
     * @param content the server content
     * @return the version information
     * @throws ServerError if any error occurs
     */
    VersionHistory resolve(final String contentType, final InputStream content) throws ServerError;

    /**
     * Add an extra resolver to the
     * existing version resolver
     *
     * @param other the other resolver
     * @return the combined version resolver
     */
    default VersionResolver resolveWith(final VersionResolver other) {
        return ((contentType, content) -> {
            VersionHistory response = null;
            ServerError suppressed = null;
            try {
                response = this.resolve(contentType, content);
            } catch (ServerError error) {
                suppressed = error;
            }
            if (response == null)
                try {
                    response = other.resolve(contentType, content);
                } catch (ServerError error) {
                    if (suppressed != null)
                        error.addSuppressed(suppressed);

                    throw error;
                }

            return response;
        });
    }
}
