package es.karmadev.versiona.data;

import es.karmadev.versiona.Version;

import java.net.URI;
import java.time.Instant;
import java.util.*;

/**
 * Represents a version information
 */
@SuppressWarnings("unused")
public final class VersionInfo {


    private final VersionHistory history;
    private final Version version;
    private final VersionChannel channel;
    private final Instant date;
    private final List<URI> updateURIs;
    private final String[] changelog;

    /**
     * Get the changelog
     *
     * @param history the version history
     * @param version the version
     * @param channel the version channel
     * @param date the version release date
     * @param updateURIs the update URIs
     * @param changelog the changelog
     */
    public VersionInfo(final VersionHistory history, final Version version, final VersionChannel channel,
                       final Instant date, final List<URI> updateURIs, final String... changelog) {
        this.history = history;
        this.version = version;
        this.channel = channel;
        this.date = date;
        this.updateURIs = updateURIs != null ? updateURIs : Collections.emptyList();
        this.changelog = (changelog != null ? changelog : new String[0]);
    }

    /**
     * Get the version history
     *
     * @return the history
     */
    public VersionHistory getHistory() {
        return this.history;
    }

    /**
     * Get the version
     *
     * @return the version
     */
    public Version getVersion() {
        return this.version;
    }

    /**
     * Get the version channel
     *
     * @return the version channel
     */
    public VersionChannel getChannel() {
        return this.channel;
    }

    /**
     * Get the version release date
     *
     * @return the release date
     */
    public Instant getDate() {
        return this.date;
    }

    /**
     * get the version update URIs
     *
     * @return the update URI
     */
    public Collection<URI> getUpdateURIs() {
        return Collections.unmodifiableCollection(this.updateURIs);
    }

    /**
     * Get the changelog
     *
     * @return the changelog
     */
    public String[] getChangelog() {
        return this.changelog.clone();
    }

    /**
     * Get the difference between
     * this version and the specified
     * version
     *
     * @param version the version
     * @return the version difference
     */
    public int getDifference(final Version version) {
        List<Version> versions = new ArrayList<>(this.history.getVersions());
        if (!versions.contains(version)) return -1;

        int thisIndex = versions.indexOf(this.version);
        int verIndex = versions.indexOf(version);

        return Math.max(thisIndex, verIndex) -
                Math.min(thisIndex, verIndex);
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link HashMap}.
     * <p>
     * The general contract of {@code hashCode} is:
     * <ul>
     * <li>Whenever it is invoked on the same object more than once during
     *     an execution of a Java application, the {@code hashCode} method
     *     must consistently return the same integer, provided no information
     *     used in {@code equals} comparisons on the object is modified.
     *     This integer need not remain consistent from one execution of an
     *     application to another execution of the same application.
     * <li>If two objects are equal according to the {@code equals(Object)}
     *     method, then calling the {@code hashCode} method on each of
     *     the two objects must produce the same integer result.
     * <li>It is <em>not</em> required that if two objects are unequal
     *     according to the {@link Object#equals(Object)}
     *     method, then calling the {@code hashCode} method on each of the
     *     two objects must produce distinct integer results.  However, the
     *     programmer should be aware that producing distinct integer results
     *     for unequal objects may improve the performance of hash tables.
     * </ul>
     * <p>
     * As much as is reasonably practical, the hashCode method defined by
     * class {@code Object} does return distinct integers for distinct
     * objects. (This is typically implemented by converting the internal
     * address of the object into an integer, but this implementation
     * technique is not required by the
     * Java&trade; programming language.)
     *
     * @return a hash code value for this object.
     * @see Object#equals(Object)
     * @see System#identityHashCode
     */
    @Override
    public int hashCode() {
        return this.version.hashCode() +
                Arrays.hashCode(this.changelog);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * <p>
     * The {@code equals} method implements an equivalence relation
     * on non-null object references:
     * <ul>
     * <li>It is <i>reflexive</i>: for any non-null reference value
     *     {@code x}, {@code x.equals(x)} should return
     *     {@code true}.
     * <li>It is <i>symmetric</i>: for any non-null reference values
     *     {@code x} and {@code y}, {@code x.equals(y)}
     *     should return {@code true} if and only if
     *     {@code y.equals(x)} returns {@code true}.
     * <li>It is <i>transitive</i>: for any non-null reference values
     *     {@code x}, {@code y}, and {@code z}, if
     *     {@code x.equals(y)} returns {@code true} and
     *     {@code y.equals(z)} returns {@code true}, then
     *     {@code x.equals(z)} should return {@code true}.
     * <li>It is <i>consistent</i>: for any non-null reference values
     *     {@code x} and {@code y}, multiple invocations of
     *     {@code x.equals(y)} consistently return {@code true}
     *     or consistently return {@code false}, provided no
     *     information used in {@code equals} comparisons on the
     *     objects is modified.
     * <li>For any non-null reference value {@code x},
     *     {@code x.equals(null)} should return {@code false}.
     * </ul>
     * <p>
     * The {@code equals} method for class {@code Object} implements
     * the most discriminating possible equivalence relation on objects;
     * that is, for any non-null reference values {@code x} and
     * {@code y}, this method returns {@code true} if and only
     * if {@code x} and {@code y} refer to the same object
     * ({@code x == y} has the value {@code true}).
     * <p>
     * Note that it is generally necessary to override the {@code hashCode}
     * method whenever this method is overridden, so as to maintain the
     * general contract for the {@code hashCode} method, which states
     * that equal objects must have equal hash codes.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     * @see #hashCode()
     * @see HashMap
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof VersionInfo)) return false;
        VersionInfo other = (VersionInfo) obj;

        if (!other.history.equals(this.history)) return false;
        return this.version.equals(other.version);
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.version).append(":\n ");

        for (int i = 0; i < this.changelog.length; i++) {
            String line = this.changelog[i];
            builder.append(line);

            if (i != this.changelog.length - 1)
                builder.append('\n');
        }

        return builder.toString();
    }
}
