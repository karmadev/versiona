package es.karmadev.versiona;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Represents a version
 */
@SuppressWarnings("unused")
public final class Version implements Comparable<Version> {

    private final static char DOT = '.';
    private final static int[] EMPTY = new int[0];

    private final int mayor;
    private final int minor;
    private final int patch;
    private final int patchMinor;
    private final String build;

    private final String raw;

    private Version(final int mayor, final int minor, final int patch,
                    final int patchMinor, final String build) {
        this.mayor = mayor;
        this.minor = minor;
        this.patch = patch;
        this.patchMinor = patchMinor;
        this.build = build;

        this.raw = buildRawVersion();
    }

    /**
     * Get the mayor version
     *
     * @return the mayor version
     */
    public int getMayor() {
        return Math.max(0, this.mayor);
    }

    /**
     * Get if the version has the
     * mayor version defined
     *
     * @return if the version has the
     * mayor version
     */
    public boolean hasMayor() {
        return this.mayor > -1;
    }

    /**
     * Get the minor version
     *
     * @return the minor version
     */
    public int getMinor() {
        return Math.max(0, this.minor);
    }

    /**
     * Get if the version has the
     * minor version defined
     *
     * @return if the version has the
     * minor version
     */
    public boolean hasMinor() {
        return this.minor > -1;
    }

    /**
     * Get the version patch
     *
     * @return the version patch
     */
    public int getPatch() {
        return Math.max(0, this.patch);
    }

    /**
     * Get if the version has
     * version patch defined
     *
     * @return if the version has the
     * version patch
     */
    public boolean hasPatch() {
        return this.patch > -1;
    }

    /**
     * Get the version minor patch
     *
     * @return the version minor patch
     */
    public int getPatchMinor() {
        return Math.max(0, this.patchMinor);
    }

    /**
     * Get if the version has
     * minor version patch defined
     *
     * @return if the version has the
     * minor version patch
     */
    public boolean hasPatchMinor() {
        return this.patchMinor > -1;
    }

    /**
     * Get the version build
     *
     * @return the build
     */
    public String getBuild() {
        return this.build;
    }

    /**
     * Get if the version has the
     * build defined
     *
     * @return if the version has the
     * build defined
     */
    public boolean hasBuild() {
        return this.build != null;
    }

    /**
     * Get if this version is before
     * the provided one
     *
     * @param other the other version
     * @return if the version is before the
     * provided one
     */
    public boolean isBefore(final Version other) {
        return this.compareTo(other) < 0;
    }

    /**
     * Get if the version is at the same
     * point as the provided one
     *
     * @param version the other version
     * @return if the versions are at the
     * same point
     */
    public boolean isSameAs(final Version version) {
        return version.compareTo(this) == 0;
    }

    /**
     * Get if this version is after
     * the provided one
     *
     * @param other the other version
     * @return if the version is after the
     * provided one
     */
    public boolean isAfter(final Version other) {
        return this.compareTo(other) > 0;
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * <p>The implementor must ensure <tt>sgn(x.compareTo(y)) ==
     * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
     * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
     * <tt>y.compareTo(x)</tt> throws an exception.)
     *
     * <p>The implementor must also ensure that the relation is transitive:
     * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
     * <tt>x.compareTo(z)&gt;0</tt>.
     *
     * <p>Finally, the implementor must ensure that <tt>x.compareTo(y)==0</tt>
     * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
     * all <tt>z</tt>.
     *
     * <p>It is strongly recommended, but <i>not</i> strictly required that
     * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
     * class that implements the <tt>Comparable</tt> interface and violates
     * this condition should clearly indicate this fact.  The recommended
     * language is "Note: this class has a natural ordering that is
     * inconsistent with equals."
     *
     * <p>In the foregoing description, the notation
     * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
     * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
     * <tt>0</tt>, or <tt>1</tt> according to whether the value of
     * <i>expression</i> is negative, zero or positive.
     *
     * @param other the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     */
    @Override
    public int compareTo(final Version other) {
        if (other == null) return -1;

        int result = Integer.compare(this.getMayor(), other.getMayor());
        if (result != 0) {
            return result;
        }

        result = Integer.compare(this.getMinor(), other.getMinor());
        if (result != 0) {
            return result;
        }

        result = Integer.compare(this.getPatch(), other.getPatch());
        if (result != 0) {
            return result;
        }

        return Integer.compare(this.getPatchMinor(), other.getPatchMinor());
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * <p>
     * The {@code equals} method implements an equivalence relation
     * on non-null object references:
     * <ul>
     * <li>It is <i>reflexive</i>: for any non-null reference value
     *     {@code x}, {@code x.equals(x)} should return
     *     {@code true}.
     * <li>It is <i>symmetric</i>: for any non-null reference values
     *     {@code x} and {@code y}, {@code x.equals(y)}
     *     should return {@code true} if and only if
     *     {@code y.equals(x)} returns {@code true}.
     * <li>It is <i>transitive</i>: for any non-null reference values
     *     {@code x}, {@code y}, and {@code z}, if
     *     {@code x.equals(y)} returns {@code true} and
     *     {@code y.equals(z)} returns {@code true}, then
     *     {@code x.equals(z)} should return {@code true}.
     * <li>It is <i>consistent</i>: for any non-null reference values
     *     {@code x} and {@code y}, multiple invocations of
     *     {@code x.equals(y)} consistently return {@code true}
     *     or consistently return {@code false}, provided no
     *     information used in {@code equals} comparisons on the
     *     objects is modified.
     * <li>For any non-null reference value {@code x},
     *     {@code x.equals(null)} should return {@code false}.
     * </ul>
     * <p>
     * The {@code equals} method for class {@code Object} implements
     * the most discriminating possible equivalence relation on objects;
     * that is, for any non-null reference values {@code x} and
     * {@code y}, this method returns {@code true} if and only
     * if {@code x} and {@code y} refer to the same object
     * ({@code x == y} has the value {@code true}).
     * <p>
     * Note that it is generally necessary to override the {@code hashCode}
     * method whenever this method is overridden, so as to maintain the
     * general contract for the {@code hashCode} method, which states
     * that equal objects must have equal hash codes.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     * @see #hashCode()
     * @see HashMap
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof Version)) return false;
        Version other = (Version) obj;

        boolean buildEquals = (this.build == null || this.build.isEmpty() ?
                other.build == null || other.build.isEmpty() : this.build.equals(other.build));

        return other.mayor == this.mayor &&
                other.minor == this.minor &&
                other.patch == this.patch && other.patchMinor == this.patchMinor &&
                buildEquals;
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return this.raw;
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link HashMap}.
     * <p>
     * The general contract of {@code hashCode} is:
     * <ul>
     * <li>Whenever it is invoked on the same object more than once during
     *     an execution of a Java application, the {@code hashCode} method
     *     must consistently return the same integer, provided no information
     *     used in {@code equals} comparisons on the object is modified.
     *     This integer need not remain consistent from one execution of an
     *     application to another execution of the same application.
     * <li>If two objects are equal according to the {@code equals(Object)}
     *     method, then calling the {@code hashCode} method on each of
     *     the two objects must produce the same integer result.
     * <li>It is <em>not</em> required that if two objects are unequal
     *     according to the {@link Object#equals(Object)}
     *     method, then calling the {@code hashCode} method on each of the
     *     two objects must produce distinct integer results.  However, the
     *     programmer should be aware that producing distinct integer results
     *     for unequal objects may improve the performance of hash tables.
     * </ul>
     * <p>
     * As much as is reasonably practical, the hashCode method defined by
     * class {@code Object} does return distinct integers for distinct
     * objects. (This is typically implemented by converting the internal
     * address of the object into an integer, but this implementation
     * technique is not required by the
     * Java&trade; programming language.)
     *
     * @return a hash code value for this object.
     * @see Object#equals(Object)
     * @see System#identityHashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.mayor, this.minor,
                this.patch, this.patchMinor, this.build);
    }

    private String buildRawVersion() {
        boolean hasBuild = this.build != null &&
                !this.build.trim().isEmpty();

        int lastIndex = (hasPatchMinor() ? 3 :
                hasPatch() ? 2 : hasMinor() ? 1 : hasMayor() ? 0 : -1);

        if (lastIndex == -1) return "0";

        StringBuilder builder = new StringBuilder(String.valueOf(
                Math.max(0, mayor)
        ));
        if (lastIndex >= 1)
            builder.append(DOT).append(
                    Math.max(0, minor)
            );

        if (lastIndex >= 2)
            builder.append(DOT).append(
                    Math.max(0, patch)
            );

        if (lastIndex == 3)
            builder.append(DOT).append(
                    Math.max(0, patchMinor)
            );

        if (hasBuild)
            builder.append('-').append(build);

        return builder.toString();
    }

    /**
     * Build a new version
     *
     * @param versions the version numbers
     * @param build the version build
     * @return the version
     */
    public static Version build(final int[] versions, final String build) {
        return build(
                getOrDefault(versions, 0),
                getOrDefault(versions, 1),
                getOrDefault(versions, 2),
                getOrDefault(versions, 3),
                build
        );
    }

    /**
     * Build a new version
     *
     * @param mayor the version mayor
     * @return the version
     */
    public static Version build(final int mayor) {
        return build(mayor, -1, -1, -1, null);
    }

    /**
     * Build a new version
     *
     * @param mayor the version mayor
     * @param minor the version minor
     * @return the version
     */
    public static Version build(final int mayor, final int minor) {
        return build(mayor, minor, -1, -1, null);
    }

    /**
     * Build a new version
     *
     * @param mayor the version mayor
     * @param minor the version minor
     * @param patch the version patch
     * @return the version
     */
    public static Version build(final int mayor, final int minor, final int patch) {
        return build(mayor, minor, patch, -1, null);
    }

    /**
     * Build a new version
     *
     * @param mayor the version mayor
     * @param minor the version minor
     * @param patch the version patch
     * @param minorPatch the version minor patch
     * @return the version
     */
    public static Version build(final int mayor, final int minor, final int patch,
                         final int minorPatch) {
        return build(mayor, minor, patch, minorPatch, null);
    }

    /**
     * Build a new version
     *
     * @param mayor the version mayor
     * @param minor the version minor
     * @param patch the version patch
     * @param minorPatch the version minor patch
     * @param build the version build
     * @return the version
     */
    public static Version build(final int mayor, final int minor, final int patch,
                         final int minorPatch, final String build) {
        return new Version(
                Math.max(-1, mayor),
                Math.max(-1, minor),
                Math.max(-1, patch),
                Math.max(-1, minorPatch),
                build
        );
    }

    /**
     * Build a version from a
     * string
     *
     * @param raw the raw version string
     * @return the version
     */
    public static Version build(final String raw) {
        int mayor = -1;
        int minor = -1;
        int patch = -1;
        int minorPatch = -1;

        String versionString = raw;
        String build = null;

        if (raw.contains("-")) {
            String[] data = raw.split("-");
            versionString = data[0];
            build = raw.substring(versionString.length() + 1);
        }

        if (versionString.contains(".")) {
            int[] info = resolve(versionString);

            mayor = getOrDefault(info, 0);
            minor = getOrDefault(info, 1);
            patch = getOrDefault(info, 2);
            minorPatch = getOrDefault(info, 3);
        }

        return build(mayor, minor, patch, minorPatch, build);
    }

    private static int[] resolve(final String ver) {
        String[] data = ver.split("\\.");
        if (data.length == 0) return EMPTY;

        List<Integer> numbers = new ArrayList<>();
        for (String str : data) {
            try {
                int number = Integer.parseInt(str);
                numbers.add(number);
            } catch (NumberFormatException ignored) {}
        }

        int[] finalResult = new int[numbers.size()];
        for (int i = 0; i < numbers.size(); i++) {
            finalResult[i] = numbers.get(i);
        }

        return finalResult;
    }

    private static int getOrDefault(final int[] array, final int index) {
        if (array.length <= index || index < 0) return -1;
        return array[index];
    }
}
