package es.karmadev.versiona.data;

import es.karmadev.versiona.Version;

import java.net.URI;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Version history
 */
@SuppressWarnings("unused")
public final class VersionHistory {

    private final Collection<VersionInfo> info = new LinkedHashSet<>();

    private VersionHistory(final Builder builder) {
        this.info.addAll(
                builder.info.keySet().stream().map((v) -> {
                    BuilderVersionData data = builder.info.get(v);
                    return new VersionInfo(this, v,
                            data.channel, data.date,
                            data.updateURIs, data.changelog);
                }).collect(Collectors.toList())
        );
    }

    /**
     * Get all the versions
     *
     * @return the versions
     */
    public Collection<Version> getVersions() {
        return this.info.stream().map(VersionInfo::getVersion)
                .collect(Collectors.toList());
    }

    /**
     * Collect all the version changelogs
     * inside the provided range
     *
     * @param from the range to start at
     * @param to the range to end at
     * @return the collected changelogs
     */
    public Collection<VersionInfo> collect(final Version from, final Version to) {
        return this.info.stream().filter((ch) -> ch.getVersion().equals(from) ||
                ch.getVersion().equals(to) || (ch.getVersion().isAfter(from) &&
                ch.getVersion().isBefore(to))).collect(Collectors.toList());
    }

    /**
     * Collect all the changelogs starting from the
     * specified version until the latest
     *
     * @param from the version to start at
     * @return the changelogs until the latest version
     */
    public Collection<VersionInfo> collectFrom(final Version from) {
        return this.info.stream().filter((ch) -> ch.getVersion().equals(from) ||
                ch.getVersion().isAfter(from)).collect(Collectors.toList());
    }

    /**
     * Collect all the changelogs starting from
     * the first version until the specified
     * one
     *
     * @param to the version to end at
     * @return the changelogs since the first version
     * to the specified version
     */
    public Collection<VersionInfo> collectTo(final Version to) {
        return this.info.stream().filter((ch) -> ch.getVersion().equals(to) ||
                ch.getVersion().isBefore(to)).collect(Collectors.toList());
    }

    /**
     * Get the latest version
     *
     * @return the latest version
     */
    public VersionInfo getLatest() {
        return this.info.stream()
                .reduce((ch1, ch2) ->
                        ch1.getVersion().isAfter(ch2.getVersion()) ? ch1 : ch2)
                .orElse(null);
    }

    /**
     * Get the first version
     *
     * @return the first version
     */
    public VersionInfo getFirst() {
        return this.info.stream()
                .reduce((ch1, ch2) ->
                        ch1.getVersion().isBefore(ch2.getVersion()) ? ch1 : ch2)
                .orElse(null);
    }

    /**
     * Get the version changelog
     *
     * @param version the version
     * @return the version changelog
     */
    public VersionInfo getChangelog(final Version version) {
        return this.info.stream().filter((ch) -> ch.getVersion().equals(version))
                .findAny().orElse(null);
    }

    /**
     * Get if the version is up-to-date
     * to version history latest
     *
     * @param current the current version
     * @return if the version is up-to-date
     */
    public boolean isUpToDate(final Version current) {
        VersionInfo latest = this.getLatest();
        if (latest == null) return false;

        Version version = latest.getVersion();
        return current.isAfter(version) || current.isSameAs(version);
    }

    /**
     * Create a new version info builder
     *
     * @return the builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link HashMap}.
     * <p>
     * The general contract of {@code hashCode} is:
     * <ul>
     * <li>Whenever it is invoked on the same object more than once during
     *     an execution of a Java application, the {@code hashCode} method
     *     must consistently return the same integer, provided no information
     *     used in {@code equals} comparisons on the object is modified.
     *     This integer need not remain consistent from one execution of an
     *     application to another execution of the same application.
     * <li>If two objects are equal according to the {@code equals(Object)}
     *     method, then calling the {@code hashCode} method on each of
     *     the two objects must produce the same integer result.
     * <li>It is <em>not</em> required that if two objects are unequal
     *     according to the {@link Object#equals(Object)}
     *     method, then calling the {@code hashCode} method on each of the
     *     two objects must produce distinct integer results.  However, the
     *     programmer should be aware that producing distinct integer results
     *     for unequal objects may improve the performance of hash tables.
     * </ul>
     * <p>
     * As much as is reasonably practical, the hashCode method defined by
     * class {@code Object} does return distinct integers for distinct
     * objects. (This is typically implemented by converting the internal
     * address of the object into an integer, but this implementation
     * technique is not required by the
     * Java&trade; programming language.)
     *
     * @return a hash code value for this object.
     * @see Object#equals(Object)
     * @see System#identityHashCode
     */
    @Override
    public int hashCode() {
        return this.info.stream()
                .map(Objects::hashCode)
                .reduce(0, Integer::sum);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * <p>
     * The {@code equals} method implements an equivalence relation
     * on non-null object references:
     * <ul>
     * <li>It is <i>reflexive</i>: for any non-null reference value
     *     {@code x}, {@code x.equals(x)} should return
     *     {@code true}.
     * <li>It is <i>symmetric</i>: for any non-null reference values
     *     {@code x} and {@code y}, {@code x.equals(y)}
     *     should return {@code true} if and only if
     *     {@code y.equals(x)} returns {@code true}.
     * <li>It is <i>transitive</i>: for any non-null reference values
     *     {@code x}, {@code y}, and {@code z}, if
     *     {@code x.equals(y)} returns {@code true} and
     *     {@code y.equals(z)} returns {@code true}, then
     *     {@code x.equals(z)} should return {@code true}.
     * <li>It is <i>consistent</i>: for any non-null reference values
     *     {@code x} and {@code y}, multiple invocations of
     *     {@code x.equals(y)} consistently return {@code true}
     *     or consistently return {@code false}, provided no
     *     information used in {@code equals} comparisons on the
     *     objects is modified.
     * <li>For any non-null reference value {@code x},
     *     {@code x.equals(null)} should return {@code false}.
     * </ul>
     * <p>
     * The {@code equals} method for class {@code Object} implements
     * the most discriminating possible equivalence relation on objects;
     * that is, for any non-null reference values {@code x} and
     * {@code y}, this method returns {@code true} if and only
     * if {@code x} and {@code y} refer to the same object
     * ({@code x == y} has the value {@code true}).
     * <p>
     * Note that it is generally necessary to override the {@code hashCode}
     * method whenever this method is overridden, so as to maintain the
     * general contract for the {@code hashCode} method, which states
     * that equal objects must have equal hash codes.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     * @see #hashCode()
     * @see HashMap
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof VersionHistory))
            return false;

        VersionHistory other = (VersionHistory) obj;
        return this.info.equals(other.info);
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (VersionInfo changelog : this.info) {
            builder.append(changelog)
                    .append('\n');
        }

        return builder.substring(0, builder.length() - 1);
    }

    /**
     * Represents a version info
     * builder
     */
    public final static class Builder {

        private final static String[] EMPTY = new String[0];
        private final Map<Version, BuilderVersionData> info = new LinkedHashMap<>();

        private Builder() {}

        /**
         * Add a version to the builder
         *
         * @param version the version to add
         * @param channel the version channel
         * @param changelog the changelog
         * @return the builder
         */
        public Builder add(final Version version,
                           final VersionChannel channel,
                           final String... changelog) {
            return this.add(version, channel, null, null, changelog);
        }

        /**
         * Add a version to the builder
         *
         * @param version the version to add
         * @param date the version date
         * @param changelog the changelog
         * @return the builder
         */
        public Builder add(final Version version, final Instant date, final String... changelog) {
            return this.add(version, VersionChannel.RELEASE, date,
                    null, changelog);
        }

        /**
         * Add a version to the builder
         *
         * @param version the version to add
         * @param channel the version channel
         * @param date the version date
         * @param changelog the changelog
         * @return the builder
         */
        public Builder add(final Version version,
                           final VersionChannel channel,
                           final Instant date,
                           final String... changelog) {
            return this.add(version, channel, date, null, changelog);
        }

        /**
         * Add a version to the builder
         *
         * @param version the version to add
         * @param channel the version channel
         * @param date the version release date
         * @param updateURIs the version update URIs
         * @param changelog the changelog
         * @return the builder
         */
        public Builder add(final Version version,
                           final VersionChannel channel,
                           final Instant date,
                           final List<URI> updateURIs,
                           final String... changelog) {
            this.info.put(version, new BuilderVersionData(channel, date,
                    updateURIs == null ? Collections.emptyList() : updateURIs,
                    (changelog == null ? EMPTY : changelog)));
            return this;
        }

        /**
         * Build the version info
         *
         * @return the version info
         */
        public VersionHistory build() {
            return new VersionHistory(this);
        }
    }

    private static class BuilderVersionData {
        public final VersionChannel channel;
        public final Instant date;
        public final List<URI> updateURIs;
        public final String[] changelog;

        private BuilderVersionData(final VersionChannel channel, final Instant date,
                                   final List<URI> updateURIs, final String... changelog) {
            this.channel = channel;
            this.date = date;
            this.updateURIs = new ArrayList<>(updateURIs);
            this.changelog = (changelog == null ? new String[0] : changelog);
        }
    }
}
