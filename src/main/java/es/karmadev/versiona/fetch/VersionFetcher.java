package es.karmadev.versiona.fetch;

import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonInstance;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.api.kson.io.JsonReader;
import es.karmadev.versiona.Version;
import es.karmadev.versiona.data.VersionChannel;
import es.karmadev.versiona.data.VersionHistory;
import es.karmadev.versiona.fetch.exception.ServerError;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Represents a version fetcher
 */
@SuppressWarnings("unused")
public final class VersionFetcher {

    private final static String JSON = "application/json";
    private final static String MARKDOWN = "text/markdown";
    private final static String BLOB = "text/plain";

    private final URL url;
    private final TimedReference<VersionHistory> cache;

    private int connectTimeout = (int) TimeUnit.SECONDS.toMillis(10);
    private int readTimeout = (int) TimeUnit.SECONDS.toMillis(30);

    private VersionResolver resolver;

    /**
     * Create a new version fetcher
     *
     * @param rawURL the raw URL
     * @throws MalformedURLException if the url is invalid
     */
    public VersionFetcher(final String rawURL) throws MalformedURLException {
        this(new URL(rawURL), 30, TimeUnit.MINUTES);
    }

    /**
     * Create a new version fetcher
     *
     * @param rawURL the raw url
     * @param cacheTime the cache time
     * @throws MalformedURLException if the url is invalid
     */
    public VersionFetcher(final String rawURL, final long cacheTime) throws MalformedURLException {
        this(new URL(rawURL), cacheTime, TimeUnit.MINUTES);
    }

    /**
     * Create a new version fetcher
     *
     * @param rawURL the raw url
     * @param cacheTime the cache time
     * @param cacheTimeUnit the cache time unit
     * @throws MalformedURLException if the url is invalid
     */
    public VersionFetcher(final String rawURL, final long cacheTime,
                          final TimeUnit cacheTimeUnit) throws MalformedURLException {
        this(new URL(rawURL), cacheTime, cacheTimeUnit);
    }

    /**
     * Create a new version fetcher
     *
     * @param url the url
     */
    public VersionFetcher(final URL url) {
        this(url, 30, TimeUnit.MINUTES);
    }

    /**
     * Create a new version fetcher
     *
     * @param url the url
     * @param cacheTime the cache time
     */
    public VersionFetcher(final URL url, final long cacheTime) {
        this(url, cacheTime, TimeUnit.MINUTES);
    }

    /**
     * Create a new version fetcher
     *
     * @param url the url
     * @param cacheTime the cache time
     * @param cacheTimeUnit the cache time unit
     */
    public VersionFetcher(final URL url, final long cacheTime,
                          final TimeUnit cacheTimeUnit) {
        this.url = url;
        this.cache = new TimedReference<>(cacheTime, cacheTimeUnit);
    }

    /**
     * Get the version fetch URL
     *
     * @return the fetch URL
     */
    public URL getFetchURL() {
        return this.url;
    }

    /**
     * Set the version resolver
     *
     * @param resolver the version resolver
     */
    public void setResolver(final VersionResolver resolver) {
        this.resolver = resolver;
    }

    /**
     * Get the version resolver
     *
     * @return the version resolver
     */
    public VersionResolver getResolver() {
        return this.resolver;
    }

    /**
     * Set the fetch connection timeout
     *
     * @param timeout the timeout
     */
    public void setConnectTimeout(final int timeout) {
        this.setConnectTimeout(timeout, TimeUnit.SECONDS);
    }

    /**
     * Set the fetch connection timeout
     *
     * @param timeout the timeout
     * @param unit the timeout time unit
     */
    public void setConnectTimeout(final int timeout, final TimeUnit unit) {
        this.connectTimeout = Math.max(1000,
                Math.min(Integer.MAX_VALUE,
                        (int) TimeUnit.MILLISECONDS.convert(timeout, unit)));
    }

    /**
     * Get the fetch connection timeout
     *
     * @return the timeout in milliseconds
     */
    public int getConnectTimeout() {
        return this.connectTimeout;
    }

    /**
     * Set the fetch read timeout
     *
     * @param timeout the timeout
     */
    public void setReadTimeout(final int timeout) {
        this.setReadTimeout(timeout, TimeUnit.SECONDS);
    }

    /**
     * Set the fetch read timeout
     *
     * @param timeout the timeout
     * @param unit the timeout time unit
     */
    public void setReadTimeout(final int timeout, final TimeUnit unit) {
        this.readTimeout = Math.max(1000,
                Math.min(Integer.MAX_VALUE,
                        (int) TimeUnit.MILLISECONDS.convert(timeout, unit)));
    }

    /**
     * Get the fetch read timeout
     *
     * @return the timeout in milliseconds
     */
    public int getReadTimeout() {
        return this.readTimeout;
    }

    /**
     * Fetch the version information
     *
     * @return the version information
     * @throws ServerError if the server returns an error instead
     */
    public VersionHistory fetch() throws ServerError {
        VersionHistory cached = cache.get();
        if (cached != null) return cached;

        try {
            URLConnection connection = url.openConnection();
            connection.setConnectTimeout(this.connectTimeout);
            connection.setReadTimeout(this.readTimeout);

            connection.connect();

            String contentType = connection.getContentType();
            switch (contentType) {
                case "application/problem+json":
                    try (InputStream stream = connection.getInputStream()) {
                        String content = readStream(stream);
                        throw new ServerError(
                                url, content
                        );
                    }
                case JSON:
                    cached = readJson(connection);
                    break;
                case MARKDOWN:
                case BLOB:
                    cached = readBase(contentType, connection);
                    break;
                default:
                    if (this.resolver != null) {
                        try (InputStream stream = connection.getInputStream()) {
                            cached = this.resolver.resolve(contentType, stream);
                        }
                    } else {
                        throw ServerError.responseType(contentType);
                    }
                    break;
            }

        } catch (IOException ex) {
            throw ServerError.wrap(ex, this.url);
        }

        if (cached == null) {
            throw new ServerError(this.url);
        }

        this.cache.update(cached);
        return cached;
    }

    private VersionHistory readJson(final URLConnection connection) throws IOException {
        try (InputStream stream = connection.getInputStream()) {
            JsonInstance instance = JsonReader.read(stream);
            if (instance == null) {
                if (this.resolver != null) {
                    return this.resolver.resolve(JSON, stream);
                } else {
                    throw ServerError.malformed(null, "Malformed json");
                }
            }

            if (!instance.isObjectType())
                if (this.resolver != null) {
                    return this.resolver.resolve(JSON, stream);
                } else {
                    throw ServerError.malformed(null, "Malformed json. Expected a json object type, but got " + instance);
                }

            JsonObject object = instance.asObject();
            if (!object.hasChild("versions") ||
                    !object.getChild("versions").isArrayType()) {
                if (this.resolver != null) {
                    return this.resolver.resolve(JSON, stream);
                } else {
                    throw ServerError.malformed(null, "Malformed json. Missing versions declaration or defined as other than array");
                }
            }

            JsonArray array = object.asArray("versions");
            return readJsonVersions(array);
        }
    }

    private VersionHistory readJsonVersions(final JsonArray array) {
        VersionHistory.Builder builder = VersionHistory.builder();

        for (JsonInstance instance : array) {
            if (!instance.isObjectType())
                throw ServerError.malformed(array.toString(), "Version array contains non-object element");

            JsonObject object = instance.asObject();
            for (String rawVersion : object.getKeys(false)) {
                JsonInstance versionData = object.getChild(rawVersion);
                if (!versionData.isObjectType())
                    throw ServerError.malformed(versionData.toString(), "Version info is not a object type");

                JsonObject versionObject = versionData.asObject();
                readVersionKey(rawVersion, versionObject, builder);
            }
        }

        return builder.build();
    }

    private void readVersionKey(final String rawVersion, final JsonObject object,
                                final VersionHistory.Builder builder) {
        if (!object.hasChild("date") || !object.hasChild("build") ||
                !object.hasChild("changelog"))
            throw ServerError.malformed(object.toString(), "Version object misses release date, build type and/or changelog");

        JsonInstance dateChild = object.getChild("date");
        JsonInstance changelogChild = object.getChild("changelog");

        if (!dateChild.isNativeType() || !changelogChild.isArrayType())
            throw ServerError.malformed(object.toString(), "Version object has invalid release date or changelog");

        JsonNative dateNative = dateChild.asNative();
        if (!dateNative.isString())
            throw ServerError.malformed(object.toString(), "Version object has invalid release date");

        Instant release = Instant.parse(dateNative.getString());
        JsonArray rawChangelog = changelogChild.asArray();

        List<String> changelog = new ArrayList<>();
        rawChangelog.forEach((chInstance) -> {
            if (!chInstance.isNativeType()) return;
            changelog.add(chInstance.asString());
        });

        Version version = Version.build(rawVersion);
        VersionChannel channel = VersionChannel.named(
                object.asString("build")
        );

        List<URI> updateURIs = new ArrayList<>();
        if (object.hasChild("update") &&
                object.getChild("update").isArrayType()) {
            JsonArray updateURLs = object.asArray("update");
            for (JsonInstance element : updateURLs) {
                if (!element.isNativeType()) continue;

                try {
                    String raw = element.asString();
                    if (raw == null) continue;
                    URI uri = new URI(raw);

                    updateURIs.add(uri);
                } catch (URISyntaxException ignored) {}
            }
        }

        builder.add(version, channel, release,
                updateURIs, changelog.toArray(new String[0]));
    }

    private VersionHistory readBase(final String contentType, final URLConnection connection) throws IOException {
        try (InputStream stream = connection.getInputStream()) {
            String content = readStream(stream);
            if (!content.contains("\n")) {
                if (this.resolver != null) {
                    return this.resolver.resolve(contentType, stream);
                } else {
                    throw ServerError.malformed(content, "Content is not split in line breaks");
                }
            }

            String[] data = content.split("\n");
            Version currentVersion = null;
            VersionChannel channel = VersionChannel.RELEASE;
            Instant date = null;
            List<String> currentChangelog = new ArrayList<>();
            List<URI> updateURIs = new ArrayList<>();

            VersionHistory.Builder builder = VersionHistory.builder();
            for (String str : data) {
                if (str.endsWith("\r"))
                    str = str.substring(0, str.length() - 1);

                if (str.startsWith("- ")) {
                    str = str.substring(2);
                    if (currentVersion == null)
                        if (this.resolver != null) {
                            return this.resolver.resolve(contentType, stream);
                        } else {
                            throw ServerError.malformed(content, "Found changelog line without a version defined first!");
                        }

                    currentChangelog.add(str);
                    continue;
                }

                if (str.startsWith("### ")) {
                    str = str.substring(4);
                    try {
                        date = Instant.parse(str);
                    } catch (DateTimeParseException ignored) {}
                    continue;
                }

                if (str.startsWith("## ")) {
                    str = str.substring(3);
                    channel = VersionChannel.named(
                            str.replaceAll("\\s", "")
                    );

                    continue;
                }

                if (str.startsWith("[") && str.endsWith("]()")) {
                    try {
                        URI uri = new URI(str.substring(1, str.length() - 3));
                        updateURIs.add(uri);
                    } catch (URISyntaxException ignored) {}
                }

                if (str.startsWith("# ")) {
                    str = str.substring(2);
                    Version version = Version.build(str);

                    handleCurrentVersion(
                            currentVersion,
                            currentChangelog,
                            updateURIs,
                            contentType,
                            stream,
                            content,
                            builder,
                            channel,
                            date
                    );

                    if (currentVersion != null) {
                        date = null;
                    }
                    currentVersion = version;
                }
            }

            handleCurrentVersion(
                    currentVersion,
                    currentChangelog,
                    updateURIs,
                    contentType,
                    stream,
                    content,
                    builder,
                    channel,
                    date
            );

            return builder.build();
        }
    }

    private void handleCurrentVersion(final Version currentVersion, final List<String> currentChangelog,
                                      final List<URI> updateURIs, final String contentType,
                                      final InputStream stream, final String content,
                                      final VersionHistory.Builder builder, final VersionChannel channel,
                                      final Instant date) {
        if (currentVersion != null) {
            if (currentChangelog.isEmpty())
                throw ServerError.malformed(content, "Found version without changelog");

            builder.add(currentVersion,
                    channel,
                    date,
                    updateURIs,
                    currentChangelog.toArray(new String[0]));

            currentChangelog.clear();
            updateURIs.clear();
        }
    }

    private String readStream(final InputStream stream) {
        try(ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            int read;
            byte[] buffer = new byte[2048];

            while ((read = stream.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }

            return out.toString();
        } catch (IOException ex) {
            throw ServerError.wrap(ex, this.url);
        }
    }
}