<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>es.karmadev.versiona</groupId>
    <artifactId>Versiona</artifactId>
    <version>1.0.0</version>
    <packaging>jar</packaging>

    <description>Versiona is a library which helps developers to manage their software versions</description>

    <licenses>
        <license>
            <name>The MIT License</name>
            <url>https://license.karmadev.es/-2025</url>
            <distribution>repo</distribution>
            <comments>
                The MIT License (MIT)
                Copyright © 2024-2025 KarmaDev

                Permission is hereby granted, free of charge, to any person obtaining a copy of this software
                and associated documentation files (the “Software”), to deal in the Software without
                restriction, including without limitation the rights to use, copy, modify, merge, publish,
                distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
                Software is furnished to do so, subject to the following conditions:

                The above copyright notice and this permission notice shall be included in all copies or
                substantial portions of the Software.

                THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
                BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
                NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
                DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
                FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
            </comments>
        </license>
    </licenses>

    <scm>
        <connection>scm:git:https://gitlab.com/karmadev/versiona.git</connection>
        <developerConnection>scm:git:https://gitlab.com/karmadev/versiona.git</developerConnection>
        <url>https://gitlab.com/karmadev/versiona</url>
        <tag>HEAD</tag>
    </scm>

    <developers>
        <developer>
            <id>karma</id>
            <name>KarmaDev</name>
            <email>karmadev.es@gmail.com</email>
            <organization>RedDo</organization>
            <organizationUrl>https://reddo.es</organizationUrl>
            <timezone>UTC+2</timezone>
            <roles>
                <role>CEO</role>
                <role>Developer</role>
            </roles>
        </developer>
    </developers>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>3.2.4</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <createDependencyReducedPom>false</createDependencyReducedPom>

                            <relocations>
                                <relocation>
                                    <pattern>es.karmadev.api.kson</pattern>
                                    <shadedPattern>es.karmadev.versiona.kson</shadedPattern>
                                </relocation>
                            </relocations>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>2.2.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.2.0</version>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <additionalOptions>
                        <additionalOption>-Xdoclint:none</additionalOption>
                    </additionalOptions>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>karmadev_snapshots</id>
            <url>https://nexus.karmadev.es/repository/snapshots/</url>
        </repository>
        <repository>
            <id>karmadev_releases</id>
            <url>https://nexus.karmadev.es/repository/internal/</url>
        </repository>
    </repositories>

    <dependencies>
        <dependency>
            <groupId>es.karmadev</groupId>
            <artifactId>KSon</artifactId>
            <version>1.0.5-SNAPSHOT</version>
            <scope>compile</scope>
        </dependency>
    </dependencies>

    <distributionManagement>
        <repository>
            <id>karmadev_releases</id>
            <url>${TARGET_RELEASES}</url>
        </repository>
        <snapshotRepository>
            <id>karmadev_snapshots</id>
            <url>${TARGET_SNAPSHOTS}</url>
        </snapshotRepository>
    </distributionManagement>
</project>