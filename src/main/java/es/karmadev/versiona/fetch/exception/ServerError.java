package es.karmadev.versiona.fetch.exception;

import java.io.IOException;
import java.net.URL;

/**
 * Represents a server error
 * while making a version fetch
 */
@SuppressWarnings("unused")
public class ServerError extends RuntimeException {

    /**
     * Initialize a server error
     *
     * @param url the URL of the connection
     * @param response the server response
     */
    public ServerError(final URL url, final String response) {
        this("Failed to perform version fetch to " + url + ": \n" + response);
    }

    /**
     * Initialize a server error
     *
     * @param url the URL of the connection
     */
    public ServerError(final URL url) {
        this("Failed to perform version fetch to " + url);
    }

    private ServerError(final String message) {
        super(message);
    }

    /**
     * Wrap an exception as a server
     * error
     *
     * @param exception the exception
     * @return the server error
     */
    public static ServerError wrap(final IOException exception, final URL url) {
        ServerError error = new ServerError("Failed to perform version fetch to " + url + ": " + exception.getMessage());
        error.addSuppressed(exception);

        return error;
    }

    /**
     * Create a malformed server response
     * error
     *
     * @param content the server content
     * @param reason the reason of why the
     *               content has been identified
     *               as malformed
     * @return the server error
     */
    public static ServerError malformed(final String content, final String reason) {
        return new ServerError("Failed to parse \n" + content + "\n" + reason);
    }

    /**
     * Create a malformed server response
     * error for the content type
     *
     * @param contentType the server returned content
     *                    type
     * @return the server error
     */
    public static ServerError responseType(final String contentType) {
        return new ServerError("Unexpected response type: " + contentType);
    }
}
